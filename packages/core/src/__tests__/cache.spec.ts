import { ValueCache } from '../cache'

describe('cache', () => {
  it('ValueCache get delete', async () => {
    const cache = new ValueCache<string, Date>({
      valueFactory: () => {
        return new Date()
      },
    })
    const value1 = await cache.get('a')
    const value2 = await cache.get('a')
    expect(value1).toBe(value2)

    const result1 = await cache.delete('a')
    expect(result1).toBe(true)
    const result2 = await cache.delete('a')
    expect(result2).toBe(false)

    const value3 = await cache.get('a')
    expect(value1).not.toBe(value3)
  })

  it('ValueCache undefined key', async () => {
    const cache = new ValueCache<string | undefined, number>({
      valueFactory: (target) => {
        return target ? Number.parseInt(target) : 0
      },
    })
    const value1 = await cache.get('1')
    expect(value1).toBe(1)
    const value2 = await cache.get(undefined)
    expect(value2).toBe(0)
  })

  it('ValueCache undefined value', async () => {
    const cache = new ValueCache<string, number | undefined>({
      valueFactory: (target) => {
        const value = Number.parseInt(target)
        return Number.isNaN(value) ? undefined : value
      },
    })
    const value1 = await cache.get('1')
    expect(value1).toBe(1)
    const value2 = await cache.get('a')
    expect(value2).toBe(undefined)
  })

  it('ValueCache WeakMap', async () => {
    type Key = {
      name: string
    }
    const cache = ValueCache.weak<Key, Date>(() => {
      return new Date()
    })
    const key1 = { name: 'a' }
    const value1 = await cache.get(key1)
    expect(value1).toBeDefined()
    const value2 = await cache.get(key1)
    expect(value1).toBe(value2)
    const value3 = await cache.get({ name: 'a' })
    expect(value2).not.toBe(value3)
  })

  it('ValueCache LRU', async () => {
    const cache = ValueCache.lru<string, Date>(
      () => {
        return new Date()
      },
      { max: 2 },
    )

    const valueA1 = await cache.get('a')
    expect(valueA1).toBeDefined()
    const valueB1 = await cache.get('b')
    expect(valueB1).toBeDefined()

    const valueA2 = await cache.get('a')
    expect(valueA1).toBe(valueA2)
    const valueC = await cache.get('c')
    expect(valueC).toBeDefined()

    const valueB2 = await cache.get('b')
    expect(valueB2).not.toBe(valueB1)
  })
})
