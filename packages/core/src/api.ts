import { GraphQLClient } from 'graphql-request'
import { getSdk, Sdk } from './generated/apiClient'
import { createGraphQLClient } from './graphql'

export type ApiService = Sdk & {
  readonly client: GraphQLClient
  setToken: (token?: string) => void
}

export function createApiService(endpoint: string, token?: string): ApiService {
  const client = createGraphQLClient(endpoint)
  const setToken = (token?: string) => {
    client.setHeader('authorization', token ? `Bearer ${token}` : '')
  }
  setToken(token)
  const sdk = getSdk(client)
  return {
    client,
    setToken,
    ...sdk,
  }
}
