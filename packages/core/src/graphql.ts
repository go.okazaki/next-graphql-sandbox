import { GraphQLClient } from 'graphql-request'
import fetch from 'cross-fetch'
import retry from 'async-retry' // https://github.com/vercel/async-retry

export function createGraphQLClient(
  endpoint: string,
  headers: Record<string, string> = {},
  retries = 1,
): GraphQLClient {
  const client = new GraphQLClient(endpoint, {
    headers,
    fetch: async (url: RequestInfo | URL, init: RequestInit) => {
      return await retry(
        async (): Promise<Response> => {
          return await fetch(url, {
            keepalive: true,
            ...init,
          })
        },
        { retries },
      )
    },
  })
  return client
}
