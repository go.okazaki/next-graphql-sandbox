export function notEmpty<T>(value: T | null | undefined): value is T {
  return value !== null && value !== undefined
}

const alphaNumericChars =
  '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

export function randomString(
  length: number,
  chars = alphaNumericChars,
): string {
  let result = ''
  const charLength = chars.length
  for (let index = 0; index < length; index++) {
    result += chars.charAt(Math.floor(Math.random() * charLength))
  }
  return result
}

export async function sleep(milliseconds: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, milliseconds))
}

export function encodeBase64<T>(value: T): string {
  return Buffer.from(JSON.stringify(value)).toString('base64')
}

export function decodeBase64<T>(value: string): T {
  return JSON.parse(Buffer.from(value, 'base64').toString('ascii'))
}
