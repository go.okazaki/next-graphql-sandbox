import { IncomingMessage, ServerResponse } from 'http'
import { Socket } from 'net'
import cookie from 'cookie'
import { generateToken, Claim, ClaimType } from '../token'
import DefaultContext from '../context'

describe('context', () => {
  it('authorization token', async () => {
    const req = new IncomingMessage(new Socket())
    const normalToken = await generateToken(
      new Claim('user', ClaimType.NORMAL),
      10,
    )
    req.headers.authorization = `Bearer ${normalToken}`
    const res = new ServerResponse(req)
    const context = new DefaultContext(req, res)
    const token = await context.getToken()
    expect(token).toBe(normalToken)
  })

  it('cookie token', async () => {
    const req = new IncomingMessage(new Socket())
    const normalToken = await generateToken(
      new Claim('user', ClaimType.NORMAL),
      10,
    )
    req.headers.cookie = cookie.serialize('token', normalToken)
    const res = new ServerResponse(req)
    const context = new DefaultContext(req, res)
    const token = await context.getToken()
    expect(token).toBe(normalToken)
  })
})
