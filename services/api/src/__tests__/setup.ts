import { HttpServerInfo, startServer } from '../app'
import { redisClient } from '../redis'

let serverInfo: HttpServerInfo

export async function getTestServer(): Promise<HttpServerInfo> {
  if (!serverInfo) {
    serverInfo = await startServer(0)
  }
  return serverInfo
}

beforeAll(async () => {
  //
})

afterAll(async () => {
  if (serverInfo) {
    await new Promise((resolve) => {
      serverInfo.server.close(resolve)
    })
  }
  await redisClient.close()
})
