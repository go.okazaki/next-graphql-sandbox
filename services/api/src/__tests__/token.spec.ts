import jwt from 'jsonwebtoken'
import { Claim, ClaimType, generateToken, verifyToken } from '../token'

describe('token', () => {
  it('generateToken', async () => {
    const payload = new Claim('test', ClaimType.NORMAL)
    const token = await generateToken(payload, 10)
    // console.log(token)
    const result = await verifyToken(token)
    // console.log(result)
    expect(result?.sub).toBe(payload.sub)
  })

  it('generateToken fake', async () => {
    const fake = jwt.sign({ sub: 'test' }, 'shhhhh')
    // console.log(fake)
    const result = await verifyToken(fake)
    expect(result).toBeUndefined()
  })
})
