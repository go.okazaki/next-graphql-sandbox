import assert from 'assert'
import { gql } from 'graphql-request'
import { testCreateUser, getTestApiService } from '../__helpers'
import { generatePrivilegeToken } from '../../token'
import { UserEdge } from '../../generated/graphql'

describe('user resolver', () => {
  it('connect admin', async () => {
    const apiService = await getTestApiService()
    const adminToken = await generatePrivilegeToken()
    apiService.setToken(adminToken)
    const email = process.env.ADMIN_EMAIL
    assert(email)
    const { connect } = await apiService.connect({ input: { email } })
    const token = connect.token
    expect(token).toBeDefined()

    apiService.setToken(token)
    const { me } = await apiService.me({})
    expect(me.id).toBeDefined()
  })

  it('connect', async () => {
    const user = await testCreateUser()
    const apiService = await getTestApiService()
    const adminToken = await generatePrivilegeToken()
    apiService.setToken(adminToken)
    const { connect } = await apiService.connect({
      input: { email: user.email },
    })
    const token = connect.token
    apiService.setToken(token)
    const { me } = await apiService.me({}, { authorization: `Bearer ${token}` })
    expect(me.id).toBe(user.id)
  })

  it('connect validation error', async () => {
    const apiService = await getTestApiService()
    const adminToken = await generatePrivilegeToken()
    apiService.setToken(adminToken)
    await expect(async () => {
      await apiService.connect({
        input: {
          email: 'xxxx', // error
        },
      })
    }).rejects.toThrowError()
  })

  it('follow', async () => {
    const user1 = await testCreateUser()
    const user2 = await testCreateUser()
    const apiService = await getTestApiService()
    const adminToken = await generatePrivilegeToken()
    apiService.setToken(adminToken)
    const { connect } = await apiService.connect({
      input: { email: user1.email },
    })
    const token = connect.token
    apiService.setToken(token)
    const { updateMe } = await apiService.updateMe({
      input: {
        addFollow: user2.id,
      },
    })
    expect(updateMe.id).toBeDefined()
    const result = await apiService.client.request(
      gql`
        query me {
          me {
            follows {
              totalCount
              edges {
                node {
                  id
                }
              }
            }
          }
        }
      `,
      {},
    )
    expect(result.me.follows.totalCount).toBeGreaterThan(0)
    expect(
      Array.from<UserEdge>(result.me.follows.edges).map((e) => e.node.id),
    ).toContain(user2.id)
  })

  it('allUsers', async () => {
    await testCreateUser()
    const apiService = await getTestApiService()
    const adminToken = await generatePrivilegeToken()
    apiService.setToken(adminToken)
    const { allUsers } = await apiService.allUsers({
      connection: {},
    })
    expect(allUsers).toBeDefined()
    expect(allUsers.edges.length).toBeGreaterThan(0)
  })
})
