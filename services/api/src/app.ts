import http from 'http'
import net from 'net'
import path from 'path'
import express from 'express'
import { ApolloServer } from 'apollo-server-express'
import { ApolloServerPlugin } from 'apollo-server-plugin-base'
import {
  ApolloServerPluginDrainHttpServer,
  ApolloServerPluginLandingPageDisabled,
  ApolloServerPluginLandingPageLocalDefault,
} from 'apollo-server-core'
import cors from 'cors'
import schema from './schema'
import DefaultContext from './context'
import { redisClient } from './redis'

const app = express()
app.disable('x-powered-by')
app.use(cors())

const apolloLoggingPlugin = (): ApolloServerPlugin => {
  return {
    async requestDidStart(requestContext) {
      if (requestContext.request.operationName === 'IntrospectionQuery') {
        return {}
      }
      process.stdout.write(
        `operationName:${requestContext.request.operationName}\n`,
      )
      return {
        async didEncounterErrors(requestContext) {
          process.stderr.write(`errors:${requestContext.errors}\n`)
        },
      }
    },
  }
}

const apolloShutdownPlugin = (): ApolloServerPlugin => {
  return {
    async serverWillStart() {
      return {
        async drainServer() {
          await redisClient.close()
        },
      }
    },
  }
}

export type HttpServerInfo = {
  server: http.Server
  addressInfo: net.AddressInfo
}

export async function startHttpServer(
  app: http.RequestListener,
  port: number,
): Promise<HttpServerInfo> {
  return new Promise((resolve) => {
    const server = http.createServer(app).listen({ port }, () => {
      const rootDir = process.cwd()
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const packageJson = require(path.join(rootDir, 'package.json'))
      const addressInfo = server.address() as net.AddressInfo
      console.log(
        `🚀 Server ${packageJson.name} version:${packageJson.version} ready at http://localhost:${addressInfo.port}/graphql`,
      )
      resolve({ server, addressInfo })
    })
  })
}

/**
 * https://www.apollographql.com/docs/apollo-server/integrations/middleware/#apollo-server-express
 */
export async function startServer(
  port = Number(process.env.PORT),
): Promise<HttpServerInfo> {
  const httpServer = http.createServer(app)
  const plugins = [
    ApolloServerPluginDrainHttpServer({ httpServer }),
    ApolloServerPluginLandingPageDisabled,
    apolloLoggingPlugin,
    apolloShutdownPlugin,
  ]
  if (process.env.NODE_ENV !== 'production') {
    plugins.push(ApolloServerPluginLandingPageLocalDefault({ embed: true }))
  }
  const server = new ApolloServer({
    schema,
    context: async ({ req, res }) => new DefaultContext(req, res),
    csrfPrevention: true,
    cache: 'bounded',
    plugins,
  })
  await server.start()
  server.applyMiddleware({ app })
  return startHttpServer(app, port)
}
