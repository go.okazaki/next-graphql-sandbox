import { IncomingMessage, ServerResponse } from 'http'
import { PrismaClient } from '@prisma/client'
import cookie from 'cookie' // https://github.com/jshttp/cookie
import { Claim, verifyToken } from './token'
import { AppAbility, toAbility } from './permission'

export interface Context {
  readonly request: IncomingMessage
  readonly response: ServerResponse
  getToken: () => Promise<string | undefined>
  getClaim: () => Promise<Claim | undefined>
  isAuthenticated: () => Promise<boolean>
  isAdmin: () => Promise<boolean>
  getPrismaClient: () => Promise<PrismaClient>
  getAbility: () => Promise<AppAbility>
}

const prisma = new PrismaClient({
  log: [
    // 'query',
    // 'info',
    'warn',
    'error',
  ],
})

export default class DefaultContext implements Context {
  private claim: Claim | undefined

  constructor(
    readonly request: IncomingMessage,
    readonly response: ServerResponse,
  ) {}

  async getToken() {
    const headers = this.request.headers
    let token = headers.authorization
      ? headers.authorization.split(' ')[1]
      : undefined
    if (!token) {
      const cookies = cookie.parse(headers.cookie || '')
      token = cookies.token
    }
    return token
  }

  async getClaim() {
    if (!this.claim) {
      const token = await this.getToken()
      if (token) {
        this.claim = await verifyToken(token)
        process.stdout.write(
          `remoteAddress:${this.request.socket.remoteAddress} sub:${this.claim?.sub}, type:${this.claim?.type}\n`,
        )
      }
    }
    return this.claim
  }

  async isAuthenticated() {
    const claim = await this.getClaim()
    return claim ? true : false
  }

  async isAdmin() {
    const claim = await this.getClaim()
    return claim?.isAdmin() ?? false
  }

  async getPrismaClient() {
    return prisma
  }

  async getAbility() {
    const claim = await this.getClaim()
    return toAbility(claim)
  }
}
