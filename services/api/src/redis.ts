import assert from 'assert'
import Redis from 'ioredis'

class RedisClient {
  private url: string | undefined
  private redis: Redis | undefined

  setUrl(url: string) {
    this.url = url
    this.redis = undefined
  }

  getUrl(): string {
    const url = this.url ?? process.env.REDIS_URL ?? 'redis://localhost:6379'
    assert(url)
    return url
  }

  async getClient(): Promise<Redis> {
    if (!this.redis) {
      this.redis = new Redis(this.getUrl(), {
        retryStrategy: function (times) {
          return Math.min(Math.exp(times), 20000)
        },
      })
    }
    return this.redis
  }

  async set(
    key: string | Buffer,
    value: string | Buffer,
    expireSec?: number,
  ): Promise<boolean> {
    const redis = await this.getClient()
    let result
    if (expireSec) {
      result = await redis.set(key, value, 'EX', expireSec)
    } else {
      result = await redis.set(key, value)
    }
    return result === 'OK'
  }

  async exists(...keys: string[] | Buffer[]): Promise<number | null> {
    const redis = await this.getClient()
    return await redis.exists(keys)
  }

  async get(key: string | Buffer): Promise<string | null> {
    const redis = await this.getClient()
    return await redis.get(key)
  }

  async getdel(key: string | Buffer): Promise<string | null> {
    // const redis = await this.getClient()
    // return await redis.getdel(key) // Redis version 6.2 or later
    const value = await this.get(key)
    if (value) {
      await this.del(key)
    }
    return value
  }

  async del(...key: (string | Buffer)[]): Promise<number> {
    const redis = await this.getClient()
    return await redis.del(key)
  }

  async close(): Promise<void> {
    const redis = this.redis
    this.redis = undefined
    if (redis) {
      await redis.quit()
    }
  }
}

export const redisClient = new RedisClient()
