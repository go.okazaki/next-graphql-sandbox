FROM node:16-alpine as build
ARG SERVICE_NAME
RUN apk add --no-cache make
COPY . /build
RUN cd /build/packages && make -j$(nproc)
RUN cd /build/services/${SERVICE_NAME} && npm i && NODE_ENV=production npm run build && npm prune --production

FROM node:16-alpine
ARG SERVICE_NAME
ENV APP_DIR=/app/services/${SERVICE_NAME}
COPY --from=build /build/packages /app/packages
COPY --from=build /build/services/${SERVICE_NAME}/node_modules ${APP_DIR}/node_modules
COPY --from=build /build/services/${SERVICE_NAME}/package.json ${APP_DIR}/package.json
COPY --from=build /build/services/${SERVICE_NAME}/.next ${APP_DIR}/.next
COPY --from=build /build/services/${SERVICE_NAME}/public ${APP_DIR}/public
COPY --from=build /build/services/${SERVICE_NAME}/next.config.js ${APP_DIR}/next.config.js
COPY --from=build /build/services/${SERVICE_NAME}/docker-entrypoint.sh ${APP_DIR}/docker-entrypoint.sh
WORKDIR ${APP_DIR}
USER node
ENTRYPOINT "${APP_DIR}/docker-entrypoint.sh"
