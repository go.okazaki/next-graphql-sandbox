import Footer from './footer'
import Navbar from './navbar'

interface Props {
  children?: React.ReactNode
}

export default function Layout({ children }: Props) {
  return (
    <>
      <Navbar />
      <hr />
      {children}
      <hr />
      <Footer />
    </>
  )
}
