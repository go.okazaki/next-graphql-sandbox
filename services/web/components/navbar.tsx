import Link from 'next/link'
import {
  AiFillCodeSandboxCircle,
  AiOutlineLogin,
  AiOutlineLogout,
} from 'react-icons/ai' // https://react-icons.github.io/react-icons/icons?name=ai
import { useSession, signIn, signOut } from 'next-auth/react'

function ProfileMenu() {
  const { data: session, status } = useSession()
  if (status === 'authenticated' && session) {
    const initial =
      session.user?.name?.charAt(1) ?? session.user?.email?.charAt(1)
    return (
      <div className="flex items-stretch">
        <div className="dropdown dropdown-hover dropdown-end">
          <label tabIndex={0} className="btn btn-ghost rounded-btn">
            <div className="avatar placeholder">
              <div className="bg-neutral-focus text-neutral-content rounded-full w-8">
                <span className="text-xs">{initial}</span>
              </div>
            </div>
          </label>
          <ul
            tabIndex={0}
            className="dropdown-content menu p-2 shadow bg-base-100 rounded-box"
          >
            <li>
              <Link href="/profile">Profile</Link>
            </li>
            <li>
              <button
                onClick={async () =>
                  signOut({ callbackUrl: `${window.location.origin}` })
                }
              >
                Logout
                <AiOutlineLogout />
              </button>
            </li>
          </ul>
        </div>
      </div>
    )
  } else {
    return (
      <div className="items-stretch">
        <button onClick={async () => signIn()} className="btn btn-outline">
          Login
          <AiOutlineLogin />
        </button>
      </div>
    )
  }
}

export default function Navbar() {
  return (
    <div className="navbar bg-base-100 sticky top-0 z-30 shadow bg-opacity-90">
      <Link
        href="/"
        tabIndex={-1}
        className="btn btn-ghost normal-case text-xl text-primary"
      >
        <AiFillCodeSandboxCircle size="36" className="mx-2" />
        <div className="hidden md:inline-block">Sandbox</div>
      </Link>
      <div className="navbar-start">
        <div className="dropdown dropdown-hover">
          <label tabIndex={0} className="btn btn-ghost lg:hidden">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h8m-8 6h16"
              />
            </svg>
          </label>
          <ul
            tabIndex={0}
            className="dropdown-content menu p-2 shadow bg-base-100 rounded-box w-52"
          >
            <li>
              <Link href="/info">Info</Link>
            </li>
            <li>
              <Link href="/member">Member</Link>
            </li>
          </ul>
        </div>
      </div>
      <div className="navbar-center hidden lg:flex">
        <ul tabIndex={0} className="menu menu-horizontal p-0">
          <li>
            <Link href="/info">Info</Link>
          </li>
          <li>
            <Link href="/member">Member</Link>
          </li>
        </ul>
      </div>
      <div className="navbar-end gap-2">
        <div className="form-control">
          <input
            tabIndex={0}
            type="text"
            placeholder="Search"
            className="input input-bordered"
          />
        </div>
        <ProfileMenu></ProfileMenu>
      </div>
    </div>
  )
}
