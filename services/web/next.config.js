/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  publicRuntimeConfig: {
    apiEndpoint: process.env.API_ENDPOINT || 'http://localhost:4000/graphql',
    apiAdminToken: process.env.API_ADMIN_TOKEN,
  },
}

module.exports = nextConfig
