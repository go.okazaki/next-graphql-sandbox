import NextAuth, { NextAuthOptions } from 'next-auth'
import GoogleProvider from 'next-auth/providers/google'
import getConfig from 'next/config'
import { getApiService } from '../../../lib/api'

// https://next-auth.js.org/configuration/providers/oauth#built-in-providers
const authOptions: NextAuthOptions = {
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID || '',
      clientSecret: process.env.GOOGLE_CLIENT_SECRET || '',
    }),
  ],
  callbacks: {
    async jwt({ token, account, profile }) {
      if (account && profile?.email) {
        const apiService = await getApiService()
        const config = getConfig()
        apiService.setToken(config.publicRuntimeConfig.apiAdminToken)
        const { connect } = await apiService.connect({
          input: { email: profile.email, useCookie: false },
        })
        token.accessToken = connect.token
      }
      return token
    },
    async session({ session, token }) {
      session.accessToken = token.accessToken?.toString()
      return session
    },
  },
}

export default NextAuth(authOptions)
