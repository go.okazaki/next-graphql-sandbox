import Head from 'next/head'

function Home() {
  return (
    <>
      <Head>
        <title>Home Page</title>
      </Head>
      <main>
        <h1>Home Page</h1>
        <div className="container m-2">
          <div className="container mx-auto m-2">
            <button className="btn">Button</button>
            <button className="btn btn-primary">Button</button>
            <button className="btn btn-secondary">Button</button>
            <button className="btn btn-accent">Button</button>
            <button className="btn btn-ghost">Button</button>
            <button className="btn btn-link">Button</button>
          </div>
          <div className="container mx-auto m-2">
            <button className="btn btn-outline">Button</button>
            <button className="btn btn-outline btn-primary">Button</button>
            <button className="btn btn-outline btn-secondary">Button</button>
            <button className="btn btn-outline btn-accent">Button</button>
          </div>
          <div className="container mx-auto m-2">
            <label htmlFor="my-modal" className="btn modal-button">
              open modal
            </label>
            <input type="checkbox" id="my-modal" className="modal-toggle" />
            <div className="modal">
              <div className="modal-box">
                <h3 className="font-bold text-lg">
                  Congratulations random Interner user!
                </h3>
                <p className="py-4">
                  Youve been selected for a chance to get one year of
                  subscription to use Wikipedia for free!
                </p>
                <div className="modal-action">
                  <label htmlFor="my-modal" className="btn">
                    Yay!
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div className="container mx-auto m-2">
            <div className="alert shadow-lg">
              <div>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  className="stroke-info flex-shrink-0 w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                  ></path>
                </svg>
                <span>12 unread messages. Tap to see.</span>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  )
}

export default Home
