import Head from 'next/head'

export default function Info() {
  return (
    <>
      <Head>
        <title>Info Page</title>
      </Head>
      <main>
        <h1>Info Page</h1>
      </main>
    </>
  )
}
